#!/usr/bin/env python3
import os
import sys
from Bio.Seq import Seq
from Bio import SeqIO
import shutil
import re
import subprocess
from argparse import ArgumentParser

### Input ### 
parser = ArgumentParser()
parser.add_argument("-r", "--result_file",
                    dest = "results_file",
                    help = "results.txt file (output from finder service).",
                    required=True)
parser.add_argument("-o", "--output_dir",
                    dest = "out_dir",
                    help = "Wanted path for Circos output (pictures).",
                    default='.')
parser.add_argument("-tmp", "--tmp_dir",
                    dest = "tmp_dir",
                    help = "Directory for storage of created input files for Circos.",
                    default = '.')
parser.add_argument("-f", "--file_dir",
                    dest = "etc_template_dir",
                    help = "Path for etc_template/ where files used by the program is stored.",
                    required = True)
parser.add_argument("-c", "--circos_path",
                    dest = "circos_path",
                    help = "Path to executable Circos program.",
                    required = True)

args = parser.parse_args()


# Adapt provided paths to absolute paths
results_file_path = os.path.abspath(args.results_file)
out_dir = os.path.abspath(args.out_dir)
tmp_dir = os.path.abspath(args.tmp_dir)
etc_template_dir = os.path.abspath(args.etc_template_dir)
circos_path = os.path.abspath(args.circos_path)



### Read in info from result.txt ###
result_file = open(results_file_path, 'r')

gene_dict = dict()
gene_legend = list()

extended_output_flag = "FALSE"
gene_flag = "FALSE"
template_flag = "FALSE"

for line in result_file:
    if line.startswith('Extended Output'):
        extended_output_flag = "TRUE"
    
    if extended_output_flag == "TRUE":
        if line.startswith('#'):
            gene = line.split(' ')[1].strip()
            
            # Add full gene name to legend list
            gene_legend += [gene]
            
            # Redefine gene names with '()' because Circos can't handle parantheses
            if '(' in gene or ')' in gene:
                gene = gene.replace('(', '')
                gene = gene.replace(')', '')
            
            # Add gene to dict
            gene_dict[gene] = dict()
            gene_dict[gene]['Template'] = ''
            gene_dict[gene]['Alignment'] = ''
            gene_dict[gene]['Query'] = ''
            
            gene_flag = "TRUE"
            
    if gene_flag == "TRUE":
        if template_flag == "TRUE":
            line = line[11:].strip('\n')
            alignment = line
            gene_dict[gene]['Alignment'] += alignment
            template_flag = "FALSE"
        
        if line.startswith('template'):
            template = line.split(' ')[1].strip()
            gene_dict[gene]['Template'] += template
            template_flag = "TRUE"
        
        if line.startswith('query'):
            query = line.split(' ')[-1].strip()
            gene_dict[gene]['Query'] += query

    
            


### Create Circos outputs ###
folder_number = 0

for gene in gene_dict:
    ## Create output folders ##
    folder_number += 1
    
    # Create folder for configuration files 
    if not os.path.exists(str(tmp_dir) + '/etc' + str(folder_number)):
        os.mkdir(str(tmp_dir) + '/etc' + str(folder_number))
        
    else:
        print(str('Error: It seems there is already a directory named etc' + str(folder_number) +'/ at ' + tmp_dir + '. Move or delete this and try again.'))
        sys.exit()
        
    # Copy template files into etc/
    source_dir = str(etc_template_dir)
    destination_dir = str(tmp_dir) + '/etc' + str(folder_number)

    source_dir_files = os.listdir(source_dir)
    
    for file in source_dir_files:
        file_path = os.path.join(source_dir,file)
        
        # Copy only files
        if os.path.isfile(file_path):
            shutil.copy(file_path,destination_dir)  
        
        
    # Create folder for data files 
    if not os.path.exists(str(tmp_dir) + '/data' + str(folder_number)):
        os.mkdir(str(tmp_dir) + '/data' + str(folder_number))
        
    else:
        print(str('Error: It seems there is already a directory named data' + str(folder_number) + '/ at ' + tmp_dir + '. Move or delete this and try again.'))
        sys.exit()
    
    
    ## DATA FILE ##
    data_file = open(str(tmp_dir) + '/data' + str(folder_number) + '/data.txt','w')
    
    # Find template and query length 
    template_length = len(gene_dict[gene]['Template'])
    query_length = len(gene_dict[gene]['Query'].strip('-'))
    
    # Find query start position (if the beginning is not aligned)
    query_start = 0
    while gene_dict[gene]['Query'][query_start] == '-':
        query_start += 1
 
    
    # Write chromosome to data file
    data_line = 'chr - ' + gene + ' ' + str(folder_number) + ' 0 ' + str(template_length) + ' green\n'
    data_file.write(data_line)
    
    data_file.close()
    
    
    
    ## HIGHLIGHTS FILE AND TICKS FILE ##
    highlights_on_ideogram_file = open(str(tmp_dir) + '/data' + str(folder_number) + '/highlights_on_ideogram.txt','w')
    highlights_beside_ideogram_file = open(str(tmp_dir) + '/data' + str(folder_number) + '/highlights_beside_ideogram.txt','w')
    ticks_file = open(str(tmp_dir) + '/etc' + str(folder_number) + '/ticks.conf', 'r')
    tick_lines = ticks_file.readlines()
    ticks_file.close()
    ticks_file = open(str(tmp_dir) + '/etc' + str(folder_number) + '/ticks.conf', 'w')
    ticks_file.writelines(tick_lines)
    
    all_not_matches = list()
    
    # Find positions where template and query is not aligned
    for i in range(len(gene_dict[gene]['Alignment'])):
        if gene_dict[gene]['Alignment'][i] != '|':
            mismatch_pos = i
            
            # Add one to match Circos index
            all_not_matches += [mismatch_pos + 1]

    
    # Write eventual mismatches between query and template to highlights file
    if len(all_not_matches) > 0:
        
        # Check if query and template is aligned in the beginning - if not, this should be gray in the output
        not_aligned_start = []
        
        j = 0
        
        # Add all not aligned positions in a row from the beginning of the template
        if all_not_matches[j] == 1:
            while j < len(all_not_matches) and all_not_matches[j] == int(all_not_matches[j+1]-1):
                not_aligned_start += [all_not_matches[j]]
                
                j += 1
            
            # Add last not aligned base pair in the beginning of query
            not_aligned_start += [all_not_matches[j]]
        
        # Check if query and template is aligned in the end - if not, this should be gray in the output
        not_aligned_end = []
        
        i = 0
        
        if all_not_matches[-1] == template_length:
            not_aligned_end += [all_not_matches[-1]]
            
            # Add all not aligned positions in a row from the end of the template
            i = 1
            while i < len(all_not_matches) and all_not_matches[-i] == int(all_not_matches[-i-1]+1):
                not_aligned_end += [all_not_matches[-i-1]]
                
                i += 1
        
        # Find all mismatches that should be red in the output (all not aligned parts that are not in the very end or beginning of the template)
        if j == 0 and i == 0:
            mismatches = all_not_matches
        elif j != 0 and i == 0:
            mismatches = all_not_matches[j+1:]
        elif j == 0 and i != 0:
            mismatches = all_not_matches[:-i]
        else:
            mismatches = all_not_matches[j+1:-i]
        
        
        # Write highlights files based on found mismatches
        start_pos = query_start
        direction = "out"
        
        # List to keep track of longer insertions and deletions where labels shouldn't be printed several times
        leave_out_labels = list()
        
        for i in range(len(mismatches)):
            mismatch_pos = mismatches[i]   
            
            # Only print ticks for longer insertions or deletions once
            if i not in leave_out_labels:
                # Print tick
                ticks_file.write('<tick>\n')
                
                # Define position and label (format based on insertion in query, deletion in query or mismatch between query and template)
                position = 'position       = ' + str(mismatch_pos) + 'b\n'
                
                # Insertions
                if gene_dict[gene]['Template'][mismatch_pos-1] == '-':
                    start_ins = str(mismatch_pos-1)
                    
                    # Find end of insertion (to be able to handle if the insertion is longer than one base)
                    k = 0
                    
                    while gene_dict[gene]['Template'][mismatch_pos - 1 + k] == '-':
                        leave_out_labels += [i+k]
                        k += 1
                        
                    end_ins = mismatch_pos-1 + k
                    
                    label = 'label	       = ' + start_ins + '_' + str(int(start_ins)+1) + 'ins' + gene_dict[gene]['Query'][int(start_ins):end_ins] + '\n'
                
                # Deletions   
                elif gene_dict[gene]['Query'][mismatch_pos-1] == '-':
                    start_del = str(mismatch_pos-1)
                    
                    # Find end of deletion (to be able to handle if the deletion is longer than one base)
                    k = 0
                    
                    while gene_dict[gene]['Query'][mismatch_pos-1 + k] == '-':
                        leave_out_labels += [i+k]
                        k += 1
                    
                    end_del = mismatch_pos-1 + k
                    
                    # Define different labels based on length of deletion (PointFinder format)
                    if k > 1:
                        label = 'label	       = ' + start_del + '_' + str(int(start_del)+1) + 'del' + gene_dict[gene]['Template'][int(start_del):end_del] + '\n'
                    
                    else:
                        label = 'label	       = ' + start_del + 'del' + gene_dict[gene]['Template'][int(start_del)] + '\n'
                
                # Mismatch
                else: 
                    label = 'label	       = ' + str(mismatch_pos) + gene_dict[gene]['Template'][mismatch_pos-1] + '>' + gene_dict[gene]['Query'][mismatch_pos-1] + '\n'
                
                ticks_file.write(position)
                ticks_file.write(label)
                
                # If mismatch is right next to another mismatch (< 0.4% of the genome away), print it on the other side of the ideogram
                if i > 0 and (int(mismatch_pos)-int(mismatches[i-1]))/template_length < 0.004:
                    if direction == "out":
                        ticks_file.write("radius               = dims(ideogram,radius_inner) - 154p\n")
                        ticks_file.write("orientation          = in\n")
                        ticks_file.write("")
                        direction = "in"
                    elif direction == "in":
                        direction = "out"
                
                # Define rest of settings (same for each hit)
                ticks_file.write('show_label     = yes\n')
                ticks_file.write('</tick>\n\n')
            
            # Print red on the ideogram to illustrate mismatch
            highlights_on_ideogram_file.write(gene + ' ' + str(mismatch_pos) + ' ' + str(mismatch_pos+1) + ' fill_color=dred\n')
            
            # Create aligned part of query and mismatches in query
            highlights_beside_ideogram_file.write(gene + ' ' + str(start_pos) + ' ' + str(mismatch_pos) + ' fill_color=green\n')
            highlights_beside_ideogram_file.write(gene + ' ' + str(mismatch_pos) + ' ' + str(mismatch_pos+1) + ' fill_color=dred\n')
            
            start_pos = mismatch_pos+1
        
        # Create the rest of the query (from last mismatch to end)
        highlights_beside_ideogram_file.write(gene + ' ' + str(start_pos) + ' ' + str(query_start + query_length) + ' fill_color=green\n')
        
        # Write gray areas to highlights file
        # Beginning of ideogram
        if len(not_aligned_start) > 0:
            highlights_on_ideogram_file.write(gene + ' ' + str(0) + ' ' + str(not_aligned_start[-1]) + ' fill_color=lgrey\n')
            
            if not_aligned_start[0] < query_start:
                highlights_beside_ideogram_file.write(gene + ' ' + str(query_start) + ' ' + str(not_aligned_start[-1]) + ' fill_color=lgrey\n')
    
        # End of ideogram
        if len(not_aligned_end) > 0:
            highlights_on_ideogram_file.write(gene + ' ' + str(not_aligned_end[-1]) + ' ' + str(template_length) + ' fill_color=lgrey\n')
            
            if not_aligned_end[-1] <= query_length:
                highlights_beside_ideogram_file.write(gene + ' ' + str(not_aligned_end[-1]) + ' ' + str(query_start + query_length) + ' fill_color=lgrey\n')
    
    
    # If no mismatches, create query in one go
    else:
        highlights_beside_ideogram_file.write(gene + ' ' + str(query_start) + ' ' + str(query_start + query_length) + ' fill_color=green\n')
    
    highlights_on_ideogram_file.close()
    highlights_beside_ideogram_file.close()
    
    ticks_file.write('</ticks>')
    ticks_file.close()

    

    ## CIRCOS CONF ##
    circos_conf = open(str(tmp_dir) + '/etc' + str(folder_number) + '/circos.conf', 'r')
    conf_lines = circos_conf.readlines()
    circos_conf.close()
    
    # Redefine directory names
    conf_lines[11] = 'karyotype = data' + str(folder_number) + '/data.txt'
    conf_lines[19] = 'file = data' + str(folder_number) + '/highlights_beside_ideogram.txt\n'
    conf_lines[25] = 'file = data' + str(folder_number) + '/highlights_on_ideogram.txt\n'
    
    # Rewrite file
    circos_conf = open(str(tmp_dir) + '/etc' + str(folder_number) + '/circos.conf', 'w')
    circos_conf.writelines(conf_lines)
    
    circos_conf.close()
    
    
    ## Run Circos ##
    command = ['perl', circos_path, '-conf', str(tmp_dir) + '/etc' + str(folder_number) + '/circos.conf', '-outputfile', 'circos' + str(folder_number),'-outputdir', str(out_dir)]
    
    subprocess.run(command)
    
    ## Change file names ##
    if os.path.exists(gene + '.png'):
        print('A png output for this gene,' + gene + ', already exists. Therefore the file name was not changed from circos' + str(folder_number) + '.png')
    else:
        os.rename('circos' + str(folder_number) + '.png', gene + '.png')
        
    if os.path.exists(gene + '.svg'):
        print('A svg output for this gene,' + gene + ', already exists. Therefore the file name was not changed from circos' + str(folder_number) + '.svg')
    else:
        os.rename('circos' + str(folder_number) + '.svg', gene + '.svg')
    


### Print legend ###
for i in range(len(gene_legend)):
    print(str(i+1) + ': ' + str(gene_legend[i]))
    
    
    


