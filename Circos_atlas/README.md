# README #

This repository contains Circos_output_creater.py and etc2/ 
Together, these can take a fasta file and the results_tab.tsv created by VirulenceFinder, ResFinder or PlasmidFinder 
based on this fasta file and create a circular visualization of the results. This visualization 
shows all contigs from the fasta file that created hits - and the positions of these hits. 

The tool is still under developement so any comments or suggestions are much appreciated. 

## Content of Repository ##
1. Circos_output_creater.py (the main script)
2. etc2/ (a directory with files used by Circos_output_creater.py)

## Installation ##
```bash
# Go to wanted location for Circos Visualization
cd /path/to/some/dir

# Clone and enter the Circos Visualization directory
git clone https://git@bitbucket.org/genomicepidemiology/Circos_visualization.git
cd Circos_visualization
```

### Dependencies ###
The program runs a separate program called Circos. Download the newest version here: http://circos.ca/software/download/circos/

Circos citation: Krzywinski, M. et al. Circos: an Information Aesthetic for Comparative Genomics. Genome Res (2009) 19:1639-1645. 


## Usage ##
The program takes a fasta file as input as well as a results_tab.tsv file from either VirulenceFinder, ResFinder or PlasmidFinder 
(https://cge.cbs.dtu.dk/services/). 

### Command line options ###
```bash
# Required arguments
-i, --infile			Input file (fasta). Same file as given to the finder service. 
-r, --result_file		results_tab.tsv created by the finder service. 
-f, --file_dir			Relative path to etc2/ 
-c, --circos_path		Path to executable Circos program.


# Optional arguments
-tmp, --tmp_dir		Relative path to directory for storage of the input files for Circos created by the program. (Default is .)
-o, --output_dir	Relative path to wanted location of Circos output. (Default is .)
-t, --threshold		Threshold for creating breaks in ideograms. Sequences without hits that are longer than 1/threshold of 
					the whole contig length will be cut out. If no threshold is provided by the user, the program will 
					find a suiting one. It is only recommended to use this option if the user is not satisfied with the threshold
					the program has determined.
```

An example: 
```bash
# Run program
python3 Circos_output_creater.py  -i /path/to/input_file.fasta -r /path/to/results_tab.tsv -f etc2/ -c circos-0.69-9/bin/circos
```

The program will create two new directories in the specified tmp location; etc/ and data/
These store input files for Circos created by the program based on the files in etc2/
They can be deleted after each run (and should be, if you want to run the program again). But always keep etc2/

The program will also create two image files in the specified output location containing the circular output from Circos. 


## Contact ##
Sofie Theisen Honore, s174352@student.dtu.dk

CGE Helpdesk, food-cgehelp@dtu.dk



