#!/usr/bin/env python3
import os
import sys
from Bio.Seq import Seq
from Bio import SeqIO
import shutil
import re
import subprocess
from argparse import ArgumentParser

### Input ### 
parser = ArgumentParser()
parser.add_argument("-i", "--infile",
                    dest = "input_file",
                    help = "FASTA input file (same as given to finder service).",
                    required=True)
parser.add_argument("-r", "--result_file",
                    dest = "results_file",
                    help = "results_tab.tsv file (output from finder service).",
                    required=True)
parser.add_argument("-o", "--output_dir",
                    dest = "out_dir",
                    help = "Wanted path for Circos output (pictures).",
                    default='.')
parser.add_argument("-tmp", "--tmp_dir",
                    dest = "tmp_dir",
                    help = "Directory for storage of created input files for Circos.",
                    default = '.')
parser.add_argument("-f", "--file_dir",
                    dest = "etc2_dir",
                    help = "Path for etc2/ where files used by the program is stored.",
                    required = True)
parser.add_argument("-c", "--circos_path",
                    dest = "circos_path",
                    help = "Path to executable Circos program.",
                    required = True)
parser.add_argument('-t', '--threshold',
                    dest = 'threshold',
                    help = 'Threshold for creating breaks in ideograms. Sequences without hits that are longer than 1/threshold of the whole contig length will be cut out.',
                    default = 'NA')

args = parser.parse_args()


# Adapt provided paths to absolute paths
input_file_path = os.path.abspath(args.input_file)
results_file_path = os.path.abspath(args.results_file)
out_dir = os.path.abspath(args.out_dir)
tmp_dir = os.path.abspath(args.tmp_dir)
etc2_dir = os.path.abspath(args.etc2_dir)
circos_path = os.path.abspath(args.circos_path)

# Define threshold
threshold = args.threshold


# Create folder for configuration files 
if not os.path.exists(str(tmp_dir) + '/etc'):
    os.mkdir(str(tmp_dir) + '/etc')
    
else:
    print(str('Error: It seems there is already a directory named etc/ at ' + tmp_dir + '. Move or delete this and try again.'))
    sys.exit()


# Create folder for data files 
if not os.path.exists(str(tmp_dir) + '/data'):
    os.mkdir(str(tmp_dir) + '/data')
    
else:
    print(str('Error: It seems there is already a directory named data/ at ' + tmp_dir + '. Move or delete this and try again.'))
    sys.exit()


# Copy relevant files from source directory to new etc/
source_dir = str(etc2_dir)
destination_dir = str(tmp_dir) + '/etc'

source_dir_files = os.listdir(source_dir)

for file in source_dir_files:
    file_path = os.path.join(source_dir,file)
    
    # Copy only files
    if os.path.isfile(file_path):
        shutil.copy(file_path,destination_dir)
        

### DATA FILE ###
# Find length of inputted sequence(s) and write them to data file
contigs_list = list(SeqIO.parse(input_file_path, "fasta"))

data_file = open(str(tmp_dir) + '/data/data.txt','w')

label = 1

# Create dict for storing information about each contig
contig_information = dict()

for contig in contigs_list:
    ID = contig.id
    # Redefine ID if it contains ":". Circos can't handle ":"s 
    if ':' in ID:
        ID = ID.replace(':','_')
    
    sequence_length = len(contig.seq)
    
    # Add sequence lengths to dict
    contig_information[ID] = dict()
    contig_information[ID]['Sequence Length'] = sequence_length
    
    # Save relationship between ID and label for legend
    contig_information[ID]['Label'] = label
    
    # Write chromosome to data file
    line = 'chr - ' + ID + ' ' + str(label) + ' 0 ' + str(sequence_length) + ' GREEN\n'
    data_file.write(line)
    
    label += 1

# Close files
data_file.close()


### HIGHLIGHTS FILE ###
# Find positions of hits and print to highlights file
highlights_file = open(str(tmp_dir) + '/data/highlights.txt','w')

# Create dict for storing information about each hit
hits_dict = dict()

label = 1

# Open results_file
results_file = open(results_file_path, 'r')

for line in results_file:
    if not line.startswith('Database'):
        # Write to highlights file
        hit = line.split('\t')
        contig = hit[4]
        ID = contig.split(' ')[0].replace(':','_') # replacement because Circos can't handle ':'s
        hit_gene = hit[1]           
        
        position = hit[5]           
        start_pos = position.split('..')[0]
        end_pos = position.split('..')[1]
        
        highlight = ID + ' ' + str(start_pos) + ' ' + str(end_pos) + ' fill_color=chr4\n'
        
        highlights_file.write(highlight)
        
        # Update dict for later use
        if ID not in hits_dict:
            hits_dict[ID] = dict()
            
        hits_dict[ID][label] = dict()
        hits_dict[ID][label]['Hit gene'] = hit_gene
        hits_dict[ID][label]['Start'] = start_pos
        hits_dict[ID][label]['End'] = end_pos
            
        label += 1
            

# Close files
results_file.close()
highlights_file.close()

# Terminate program if there were no hits
if len(hits_dict) == 0:
    print("Since there were no hits in the uploaded file, no Circos output will be made.")
    sys.exit(1)


### CIRCOS.CONF ###
# Edit circos.conf
conf_file = open(str(tmp_dir) + '/etc/circos.conf','r')
lines = conf_file.readlines()
conf_file.close()

## Edit chromosomes line (to determine which chromosomes are shown - only the ones with hits) and print legend ##
chromosomes = 'chromosomes = '

for ID in hits_dict:
    chromosomes += str(ID) + ';'
    
    # Print legend 
    print(str(str(contig_information[ID]['Label']) + ': ' + str(ID)))
    
chromosomes += '\n'
lines[14] = chromosomes


## Find appropriate threshold for creating breaks in the ideograms - if no threshold has been given by the user ##
# Based on ratio between full plotted length and length of all hits - should be > 0.05 (for most files - for some this will be too small)
if threshold == 'NA':
    # Find ratio
    all_hits_length = 0
    full_plotted_length = 0
    
    for ID in hits_dict:
        sequence_length = int(contig_information[ID]['Sequence Length'])
        full_plotted_length += sequence_length
        
        for hit in hits_dict[ID]:
            hit_length = int(hits_dict[ID][hit]['End']) - int(hits_dict[ID][hit]['Start'])
            
            all_hits_length += hit_length
    
    ratio = all_hits_length/full_plotted_length
    
    # Find threshold
    threshold = 1
    
    while (ratio < 0.05):
        threshold += 1
        
        full_plotted_length = 0
        
        # Find new plotted length
        for ID in hits_dict:
            # Update full plotted length
            sequence_length = contig_information[ID]['Sequence Length']
            full_plotted_length += sequence_length
            
            # Define and update list with all hit positions in each entry (ID)
            ID_positions = list()
            ID_positions += [[0,0]]
        
            for hit in hits_dict[ID]:
                hit_positions = [int(hits_dict[ID][hit]['Start']),int(hits_dict[ID][hit]['End'])]
                
                ID_positions += [hit_positions]
            
            # Sort ID_positions
            ID_positions = sorted(ID_positions)
            
            # Define breaks in the ideograms if two hits are far enough from each other (defined by threshold)
            ID_positions += [[sequence_length,sequence_length]]
            limit = sequence_length/threshold
            
            for i in range(len(ID_positions)-1):
                if (ID_positions[i+1][0] - ID_positions[i][1]) > limit:
                    # For breaks in beginning or end of entry
                    if i == 0:
                        this_break = str(ID_positions[i][1]) + '-' + str(round(ID_positions[i+1][0]-(limit/10)))
                        
                    elif i == (len(ID_positions)-2):
                        this_break = str(round(ID_positions[i][1]+(limit/10))) + '-' + str(ID_positions[i+1][0]+1)
                    
                    # All other breaks
                    else:
                        this_break = str(round(ID_positions[i][1]+(limit/10))) + '-' + str(round(ID_positions[i+1][0]-(limit/10)))
                        
                    
                    # Update full length based on breaks
                    break_start = int(this_break.split('-')[0])
                    break_end = int(this_break.split('-')[1])
                    break_length = break_end - break_start
                    
                    # Update full length
                    full_plotted_length -= break_length
            
            ratio = all_hits_length/full_plotted_length


# Print threshold that will be used (either as defined by user or by while loop above.)
print("Threshold used: ", str(threshold))


## Edit chromosomes_breaks line (to determine what part(s) of the chromosome(s) is/are shown) ##
chromosomes_break = 'chromosomes_breaks = '

for ID in hits_dict:
    # Define and update list with all hit positions in each entry (ID)
    ID_positions = list()
    ID_positions += [[0,0]]
    
    for hit in hits_dict[ID]:
        hit_positions = [int(hits_dict[ID][hit]['Start']),int(hits_dict[ID][hit]['End'])]
        
        ID_positions += [hit_positions]
    
    # Sort ID_positions
    ID_positions = sorted(ID_positions)
    
    # Create variabel to store number of ideograms that each entry will be splitted into
    contig_information[ID]['Number of Ideograms'] = 1
    
    # Create list to store break lengths in contig_information dict (to define spacing and multipliers later)
    contig_information[ID]['Breaks'] = list()
    
    # Define breaks in the ideograms if two hits are far enough from each other (defined by threshold found above or provided by user)
    sequence_length = contig_information[ID]['Sequence Length']
    ID_positions += [[sequence_length,sequence_length]]
    limit = sequence_length/int(threshold)
    
    for i in range(len(ID_positions)-1):
        if (ID_positions[i+1][0] - ID_positions[i][1]) > limit:
            # For breaks in beginning or end of entry
            if i == 0:
                this_break = str(ID_positions[i][1]) + '-' + str(round(ID_positions[i+1][0]-(limit/10)))
                
            elif i == (len(ID_positions)-2):
                this_break = str(round(ID_positions[i][1]+(limit/10))) + '-' + str(ID_positions[i+1][0]+1)
            
            # All other breaks
            else:
                this_break = str(round(ID_positions[i][1]+(limit/10))) + '-' + str(round(ID_positions[i+1][0]-(limit/10)))
                
                # Save number of ideograms this entry will be splitted into
                contig_information[ID]['Number of Ideograms'] += 1
            
            # Update breaks line for config file
            chromosomes_break += '-' + str(ID) + ':' + this_break + ';'
            
            # Save length of breaks to later define spacing and multipliers for each contig
            break_start = int(this_break.split('-')[0])
            break_end = int(this_break.split('-')[1])
            break_length = break_end - break_start
            contig_information[ID]['Breaks'] += [break_length]

chromosomes_break += '\n'
lines[15] = chromosomes_break


## Edit chromosomes_scale line ##
# Variabel for storing cumulative length of the plotted ideograms (minus breaks)
full_plotted_length = 0

# Variabel for storing full length of plotted ideograms (inclusive breaks)
full_length = 0

# Find length of each plotted ideogram
for ID in hits_dict:
    # Original sequence length
    sequence_length = contig_information[ID]['Sequence Length']
    
    full_length += sequence_length
    
    # Substract break lengths (as these are not plotted)
    if 'Breaks' in contig_information[ID]:
        for break_length in contig_information[ID]['Breaks']:
            sequence_length -= break_length
            
    # Save plotted sequence length for later
    contig_information[ID]['Plotted Length'] = sequence_length
    
    # Update cumulative length of all plotted sequences
    full_plotted_length += sequence_length


# Scaling line
chromosomes_scale = 'chromosomes_scale = '

for ID in hits_dict:
    # Define scaling of this entry's ideograms based on their cumulative length, the full plotted length and the number of ideograms this entry is split into
    scaling = (contig_information[ID]['Plotted Length']/full_plotted_length)/contig_information[ID]['Number of Ideograms']
    
    # Circos can't handle scaling = 1, so redefine this
    if scaling == 1.0:
        scaling = 0.99
    
    chromosomes_scale += str(ID + ':' + str(scaling) + 'r;')

chromosomes_scale += '\n'
lines[18] = chromosomes_scale


## Write new conf file ##
conf_file = open(str(tmp_dir) + '/etc/circos.conf','w')
conf_file.writelines(lines)
conf_file.close()

    

### TICKS FILE ###
# Collect information from ticks file
ticks_file = open(str(tmp_dir) + '/etc/ticks.conf','r')
lines = ticks_file.readlines()
ticks_file.close()

# Write original information to ticks file (so this is not overwritten by specific info written later)
ticks_file = open(str(tmp_dir) + '/etc/ticks.conf','w')
ticks_file.writelines(lines)

# Write local tick settings (specific for each entry)
for entry in hits_dict:
    # Define general ticks for this entry
    ticks_file.write('<tick>\n')
    ticks_file.write(str('chromosomes =' + entry + '\n'))
    ticks_file.write('show_label = yes\n')
    ticks_file.write('orientation = out\n')
    ticks_file.write('label_offset = 0.5r\n')
    
    # Spacing based on how much space the entry takes up out of the full circle
    if contig_information[entry]['Plotted Length']/full_plotted_length < 1/12:
        spacing = 0.6
    elif 1/12 <= contig_information[entry]['Plotted Length']/full_plotted_length < 1/8:
        spacing = 0.3
    elif 1/8 <= contig_information[entry]['Plotted Length']/full_plotted_length < 1/3:
        spacing = 0.2
    else:
        spacing = 0.1
    
    ticks_file.write('spacing_type = relative\n')
    ticks_file.write(str('rspacing = ' + str(spacing) + '\n'))
    
    
    # Define and write multiplier based on sequence length
    if contig_information[ID]['Sequence Length'] < 1000000:
        multiplier = '1'
        suffix = ''
    else:
        multiplier = '1e-3'
        suffix = 'kb'
    
        
    ticks_file.write(str('multiplier = ' + multiplier + '\n'))
    ticks_file.write(str('suffix = ' + suffix + '\n'))
    ticks_file.write('</tick>\n\n')
        
        
    # Define ticks showing hit names
    for hit in hits_dict[entry]:
        ticks_file.write('<tick>\n')
        
        # Define chromosomes
        chromosomes = 'chromosomes = ' + entry + '\n'
        ticks_file.write(chromosomes)
        
        # Define position and label
        position = 'position       = ' + hits_dict[entry][hit]['Start'] + 'b\n'
        label = 'label	       = ' + hits_dict[entry][hit]['Hit gene'] + '\n'
        ticks_file.write(position)
        ticks_file.write(label)
        
        # Define rest of settings (same for each hit)
        ticks_file.write('show_label     = yes\n')
        ticks_file.write('orientation    = in\n')
        ticks_file.write('radius         = dims(ideogram,radius_inner)\n')
        ticks_file.write('label_offset   = 120p\n') 
        ticks_file.write('label_size     = 30p\n')
        ticks_file.write('size	       = 0p\n')
        ticks_file.write('thickness      = 0p\n')
        ticks_file.write('</tick>\n\n')


ticks_file.write('</ticks>')
ticks_file.close()


### Run Circos ###
command = ['perl', circos_path, '-conf', str(tmp_dir) + '/etc/circos.conf', '-outputdir', str(out_dir)]

subprocess.run(command)




